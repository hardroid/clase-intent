package cl.duoc.clase2;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MenuPrincipalActivity extends AppCompatActivity {

    private Button btnLlamada;
    private Button btnSMS;
    private Button btnWeb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        btnLlamada = (Button)findViewById(R.id.btnLlamada);
        btnSMS = (Button)findViewById(R.id.btnSMS);
        btnWeb = (Button)findViewById(R.id.btnWeb);

        btnLlamada.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "+5697774512"));
                startActivity(intent);
            }
        });

        btnSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentt = new Intent(Intent.ACTION_VIEW);
                intentt.setData(Uri.parse("sms:"));
                intentt.setType("vnd.android-dir/mms-sms");
                intentt.putExtra(Intent.EXTRA_TEXT, "");
                intentt.putExtra("address",  "+5692151545");
                startActivity(intentt);
            }
        });

        btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "http://www.duoc.cl";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

    }


}
