package cl.duoc.clase2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends AppCompatActivity {

    private EditText txtUsuario;
    private EditText txtClave;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsuario = (EditText)findViewById(R.id.txtUsuario);
        txtClave = (EditText)findViewById(R.id.txtClave);
        btnEntrar = (Button)findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validarUsuario();
            }
        });

    }

    private void validarUsuario() {

        if(txtUsuario.getText().toString().equals("admin") && txtClave.getText().toString().equals("admin")){
            Intent i = new Intent(this, MenuPrincipalActivity.class);
            startActivity(i);
        }else{
            String mensaje = getResources().getString(R.string.mensaje_error_login);
            Toast.makeText(LoginActivity.this, mensaje, Toast.LENGTH_LONG).show();
        }

    }


}
